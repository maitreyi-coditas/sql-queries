---------------------------------------------------- CREATE -------------------------------------------------------------

mysql> CREATE TABLE users(user_id int not null primary key,
    -> first_name varchar(50),
    -> last_name varchar(50),
    -> email varchar(50),
    -> is_verified boolean,
    -> registration DATETIME DEFAULT CURRENT_TIMESTAMP);
Query OK, 0 rows affected (0.03 sec)


---------------------------------------------------- INSERT ------------------------------------------------------------

mysql> insert into users(user_id,first_name, last_name,email, is_verified) values ( 1, "Maitreyi", "Kalantre", "maitreyi@gmail.com", true); GO
Query OK, 1 row affected (0.01 sec)


mysql> select * from users;
+---------+------------+-----------+--------------------+-------------+---------------------+
| user_id | first_name | last_name | email              | is_verified | registration        |
+---------+------------+-----------+--------------------+-------------+---------------------+
|       1 | Maitreyi   | Kalantre  | maitreyi@gmail.com |           1 | 2023-02-20 09:47:28 |
+---------+------------+-----------+--------------------+-------------+---------------------+
1 row in set (0.00 sec)


mysql> Select * from users;
+---------+------------+-----------+--------------------+-------------+---------------------+
| user_id | first_name | last_name | email              | is_verified | registration        |
+---------+------------+-----------+--------------------+-------------+---------------------+
|       1 | Maitreyi   | Kalantre  | maitreyi@gmail.com |           1 | 2023-02-20 09:47:28 |
+---------+------------+-----------+--------------------+-------------+---------------------+
1 row in set (0.00 sec)


---------------------------------------------------- ALIAS ---------------------------------------------------------------


mysql> select email,first_name AS "FIRST NAME",last_name AS "LAST NAME" from users;
+--------------------+------------+-----------+
| email              | FIRST NAME | LAST NAME |
+--------------------+------------+-----------+
| maitreyi@gmail.com | Maitreyi   | Kalantre  |
+--------------------+------------+-----------+
1 row in set (0.00 sec)


------------------------------------------------- UPDATE COLUMN VALUE ----------------------------------------------------

mysql> update users set email = 'maitreyikalantre@gmail.com';
Query OK, 1 row affected (0.01 sec)
Rows matched: 1  Changed: 1  Warnings: 0

mysql> select * from users;
+---------+------------+-----------+----------------------------+-------------+---------------------+
| user_id | first_name | last_name | email                      | is_verified | registration        |
+---------+------------+-----------+----------------------------+-------------+---------------------+
|       1 | Maitreyi   | Kalantre  | maitreyikalantre@gmail.com |           1 | 2023-02-20 09:47:28 |
+---------+------------+-----------+----------------------------+-------------+---------------------+
1 row in set (0.00 sec)

mysql> insert into users(user_id,first_name, last_name,email, is_verified) values ( 2, "Sheetal", "Kalantre", "sheetal@gmail.com", true); GO
Query OK, 1 row affected (0.01 sec)


------------------------------------------------- DELETE A USER ----------------------------------------------------------

mysql> delete from users where user_id = 4;
Query OK, 1 row affected (0.01 sec)

mysql> select * from users;
+---------+------------+-----------+----------------------------+-------------+---------------------+
| user_id | first_name | last_name | email                      | is_verified | registration        |
+---------+------------+-----------+----------------------------+-------------+---------------------+
|       1 | Maitreyi   | Kalantre  | maitreyikalantre@gmail.com |           1 | 2023-02-20 09:47:28 |
|       2 | Sheetal    | Kalantre  | sheetal@gmail.com          |           1 | 2023-02-20 09:56:56 |
|       3 | Pranay     | Dongre    | pranay5@gmail.com          |           1 | 2023-02-20 09:58:43 |
+---------+------------+-----------+----------------------------+-------------+---------------------+
3 rows in set (0.00 sec)

mysql> select * from users;
+---------+------------+-----------+----------------------------+-------------+---------------------+
| user_id | first_name | last_name | email                      | is_verified | registration        |
+---------+------------+-----------+----------------------------+-------------+---------------------+
|       1 | Maitreyi   | Kalantre  | maitreyikalantre@gmail.com |           1 | 2023-02-20 09:47:28 |
|       2 | Sheetal    | Kalantre  | sheetal@gmail.com          |           1 | 2023-02-20 09:56:56 |
|       3 | Pranay     | Dongre    | pranay5@gmail.com          |           1 | 2023-02-20 09:58:43 |
|       4 | Aishwarya  | Deshpande | aishwarya@gmail.com        |           0 | 2023-02-20 10:03:51 |
+---------+------------+-----------+----------------------------+-------------+---------------------+
4 rows in set (0.00 sec)

mysql> delete from users where is_verified = FALSE;
Query OK, 1 row affected (0.01 sec)

mysql> select * from users;
+---------+------------+-----------+----------------------------+-------------+---------------------+
| user_id | first_name | last_name | email                      | is_verified | registration        |
+---------+------------+-----------+----------------------------+-------------+---------------------+
|       1 | Maitreyi   | Kalantre  | maitreyikalantre@gmail.com |           1 | 2023-02-20 09:47:28 |
|       2 | Sheetal    | Kalantre  | sheetal@gmail.com          |           1 | 2023-02-20 09:56:56 |
|       3 | Pranay     | Dongre    | pranay5@gmail.com          |           1 | 2023-02-20 09:58:43 |
+---------+------------+-----------+----------------------------+-------------+---------------------+
3 rows in set (0.00 sec)

