
CONNECT ORCALE

SQL> connect
Enter user-name: system
Enter password:
Connected.
SQL>

--------------------------------------------------------------------------------------------------------------------------

USE DUAL

SQL> select 'Hello world' from dual;

'HELLOWORLD
-----------
Hello world


SQL> select 5+5 from dual;

       5+5
----------
        10


SQL> select 'TRUE' from dual where 'Caps' = 'Caps';

'TRU
----
TRUE


SQL> select 'TRUE' from dual where 'Caps' = 'caps';

no rows selected


SQL> select systimestamp, 'hello World', 5+5 from dual;

SYSTIMESTAMP
---------------------------------------------------------------------------
'HELLOWORLD        5+5
----------- ----------
19-FEB-23 07.26.21.962000 PM +05:30
hello World         10



