INSERT DATA and SELECTION OF ALL DATA

SQL> insert into users values(1,'Maitreyi');

1 row created.

SQL> select * from users;

   USER_ID USERNAME
---------- -------------------------
         1 Maitreyi

SQL> insert into projects values (23,'WebApplication','Maitreyi');

1 row created.

SQL> select * from projects;

PROJECT_ID PROJECT_NAME              CREATOR
---------- ------------------------- -------------------------
        23 WebApplication            Maitreyi


SQL> insert into project_users values(1,23);

1 row created.

SQL> select * from project_users;

   USER_ID PROJECT_ID
---------- ----------
         1         23


SQL> insert into project_users values(1,23);

1 row created.

SQL> select * from project_users;

   USER_ID PROJECT_ID
---------- ----------
         1         23
                   
