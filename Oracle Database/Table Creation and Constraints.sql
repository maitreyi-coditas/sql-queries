//TABLE CREATION

SQL> create table users ( user_id number,username varchar2(25 char) not null unique,constraint users_pk primary key(user_id));

Table created.

SQL> desc users;
 Name                                      Null?    Type
 ----------------------------------------- -------- ----------------------------
 USER_ID                                   NOT NULL NUMBER
 USERNAME                                  NOT NULL VARCHAR2(25 CHAR)



SQL> create table projects (project_id number, project_name varchar2(25 char) unique, creator varchar(25 char) not null, constraint projects_pk primary key(project_id), constraint project_users_fk foreign key(creator) references users(username));

Table created.

SQL> desc projects;
 Name                                      Null?    Type
 ----------------------------------------- -------- ----------------------------
 PROJECT_ID                                NOT NULL NUMBER
 PROJECT_NAME                                       VARCHAR2(25 CHAR)
 CREATOR                                   NOT NULL VARCHAR2(25 CHAR)



SQL> create table project_users
  2  (
  3  user_id not null references users(user_id) on delete set null,
  4  project_id references projects(project_id) on delete set null,
  5  constraint project_users_pk primary key(user_id,project_id)
  6  );

Table created.

SQL> desc project_users;
 Name                                      Null?    Type
 ----------------------------------------- -------- ----------------------------
 USER_ID                                   NOT NULL NUMBER
 PROJECT_ID                                NOT NULL NUMBER

