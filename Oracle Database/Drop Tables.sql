//DROP TABLES

SQL> drop table users;

Table dropped.


SQL> drop table projects;

Table dropped.


SQL> drop table project_users;

Table dropped.


//DELETION OF FOREIGN KEY

SQL> delete from users where user_id = 1;
delete from users where user_id = 1
*
ERROR at line 1:
ORA-02292: integrity constraint (SYSTEM.PROJECT_USERS_FK) violated - child
record found

// ON DELETE SET NULL / DELETE CASCADE to avoid this

SQL> drop table projects;

Table dropped.

SQL> create table projects (project_id number, project_name varchar2(25 char) unique, creator varchar(25 char) not null, constraint projects_pk primary key(project_id), constraint project_users_fk foreign key(creator) references users(username) on delete set null);

Table created.

SQL> delete from users where user_id = 1;

1 row deleted.

SQL> select * from users;

no rows selected