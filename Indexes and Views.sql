// CREATE AND DROP INDEX

mysql> create index animal_species_index on animals(species_id);
Query OK, 0 rows affected (0.05 sec)
Records: 0  Duplicates: 0  Warnings: 0

mysql> drop index animal_species_index on animals;
Query OK, 0 rows affected (0.01 sec)
Records: 0  Duplicates: 0  Warnings: 0