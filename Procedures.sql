
mysql> select * from pets;
+--------+-----------+-----------+------------------+------+------------+-------+
| pet_id | name      | owner     | species          | sex  | birth      | death |
+--------+-----------+-----------+------------------+------+------------+-------+
|      1 | Snowflake | Maitreyi  | Golden Retriever | F    | 2015-01-22 | NULL  |
|      2 | Cuddles   | Sheetal   | Labrador         | M    | 2015-05-26 | NULL  |
|      3 | Tweety    | Atul      | parrot           | F    | 2022-11-29 | NULL  |
|      4 | Olaf      | Aishwarya | Kitten           | F    | 2023-02-19 | NULL  |
+--------+-----------+-----------+------------------+------+------------+-------+
4 rows in set (0.00 sec)

--------------------------------------------------------------------------------------------------------------------------

mysql> DELIMITER &&
mysql> Create Procedure pets_female()  Begin  Select * from pets where sex='F';  End &&
Query OK, 0 rows affected (0.02 sec)


mysql> call pets_female();
+--------+-----------+-----------+------------------+------+------------+-------+
| pet_id | name      | owner     | species          | sex  | birth      | death |
+--------+-----------+-----------+------------------+------+------------+-------+
|      1 | Snowflake | Maitreyi  | Golden Retriever | F    | 2015-01-22 | NULL  |
|      3 | Tweety    | Atul      | parrot           | F    | 2022-11-29 | NULL  |
|      4 | Olaf      | Aishwarya | Kitten           | F    | 2023-02-19 | NULL  |
+--------+-----------+-----------+------------------+------+------------+-------+
3 rows in set (0.01 sec)

Query OK, 0 rows affected (0.02 sec)


--------------------------------------------------------------------------------------------------------------------------

mysql> DELIMITER &&
mysql> Create Procedure pets_female_with_birthdate()
    -> Begin
    ->
    -> SET @birthdate := "2015-01-22";
    ->
    -> Select * from pets where sex = 'F' AND birth = @birthdate;
    ->
    -> End &&
Query OK, 0 rows affected (0.01 sec)


mysql> call pets_female_with_birthdate();
+--------+-----------+----------+------------------+------+------------+-------+
| pet_id | name      | owner    | species          | sex  | birth      | death |
+--------+-----------+----------+------------------+------+------------+-------+
|      1 | Snowflake | Maitreyi | Golden Retriever | F    | 2015-01-22 | NULL  |
+--------+-----------+----------+------------------+------+------------+-------+
1 row in set (0.00 sec)

Query OK, 0 rows affected (0.01 sec)
