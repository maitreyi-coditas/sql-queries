mysql> use sqltutorial;
Database changed

--------------------------------------------------------------------------------------------------------------------------

mysql> create table pets(pet_id int primary key auto_increment, name varchar(25), owner varchar(50), species varchar(25), sex char(1), birth Date, death date);
Query OK, 0 rows affected (0.04 sec)

mysql> desc pets;
+---------+-------------+------+-----+---------+----------------+
| Field   | Type        | Null | Key | Default | Extra          |
+---------+-------------+------+-----+---------+----------------+
| pet_id  | int         | NO   | PRI | NULL    | auto_increment |
| name    | varchar(25) | YES  |     | NULL    |                |
| owner   | varchar(50) | YES  |     | NULL    |                |
| species | varchar(25) | YES  |     | NULL    |                |
| sex     | char(1)     | YES  |     | NULL    |                |
| birth   | date        | YES  |     | NULL    |                |
| death   | date        | YES  |     | NULL    |                |
+---------+-------------+------+-----+---------+----------------+
7 rows in set (0.01 sec)


--------------------------------------------------------------------------------------------------------------------------

mysql> insert into pets(name,owner,species,sex,birth,death) values ('Snowflake','Maitreyi','Golden Retriever','F','2015-01-22',NULL);
Query OK, 1 row affected (0.01 sec)

mysql> insert into pets(name,owner,species,sex,birth,death) values ('Cuddles','Sheetal','Labrador','M','2015-05-26',NULL);
Query OK, 1 row affected (0.01 sec)

mysql> select * from pets;
+--------+-----------+----------+------------------+------+------------+-------+
| pet_id | name      | owner    | species          | sex  | birth      | death |
+--------+-----------+----------+------------------+------+------------+-------+
|      1 | Snowflake | Maitreyi | Golden Retriever | F    | 2015-01-22 | NULL  |
|      2 | Cuddles   | Sheetal  | Labrador         | M    | 2015-05-26 | NULL  |
+--------+-----------+----------+------------------+------+------------+-------+
2 rows in set (0.00 sec)

--------------------------------------------------------------------------------------------------------------------------
//SET 

mysql> set @id := 1;
Query OK, 0 rows affected (0.00 sec)

mysql> select @id;
+------+
| @id  |
+------+
|    1 |
+------+
1 row in set (0.00 sec)

mysql> select * from pets where pet_id = @id;
+--------+-----------+----------+------------------+------+------------+-------+
| pet_id | name      | owner    | species          | sex  | birth      | death |
+--------+-----------+----------+------------------+------+------------+-------+
|      1 | Snowflake | Maitreyi | Golden Retriever | F    | 2015-01-22 | NULL  |
+--------+-----------+----------+------------------+------+------------+-------+
1 row in set (0.00 sec)



