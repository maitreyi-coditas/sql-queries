// Multiline Comment

mysql> /*
   /*> This
   /*> is
   /*> a
   /*> multi
   /*> line
   /*> comment
   /*> */

------------------------------------------------------ TABLE CREATION ---------------------------------------------------

mysql> create table species(
    ->
    -> species_id int primary key auto_increment,
    -> species varchar(25) not null unique,
    -> friendly_name varchar(25) not null
    ->
    -> );
Query OK, 0 rows affected (0.04 sec)


mysql> desc species;
+---------------+-------------+------+-----+---------+----------------+
| Field         | Type        | Null | Key | Default | Extra          |
+---------------+-------------+------+-----+---------+----------------+
| species_id    | int         | NO   | PRI | NULL    | auto_increment |
| species       | varchar(25) | NO   | UNI | NULL    |                |
| friendly_name | varchar(25) | NO   |     | NULL    |                |
+---------------+-------------+------+-----+---------+----------------+
3 rows in set (0.00 sec)
--------------------------------------------------------------------------------------------------------------------------

mysql> create table animals(
    ->
    -> animal_id int primary key auto_increment,
    -> name varchar(25) not null,
    -> species_id int not null references species(species_id),
    -> contact_email varchar(25) not null unique
    ->
    -> );
Query OK, 0 rows affected (0.04 sec)


mysql> desc animals;
+---------------+-------------+------+-----+---------+----------------+
| Field         | Type        | Null | Key | Default | Extra          |
+---------------+-------------+------+-----+---------+----------------+
| animal_id     | int         | NO   | PRI | NULL    | auto_increment |
| name          | varchar(25) | NO   |     | NULL    |                |
| species_id    | int         | NO   |     | NULL    |                |
| contact_email | varchar(25) | NO   | UNI | NULL    |                |
+---------------+-------------+------+-----+---------+----------------+
4 rows in set (0.00 sec)

--------------------------------------------------------------------------------------------------------------------------

mysql> create table interests(
    -> animal_id int not null,
    -> species_id int not null,
    -> constraint pk_interests primary key (animal_id, species_id),
    -> constraint fk_interests_animals foreign key(animal_id) references animals(animal_id),
    -> constraint fk_interest_species foreign key(species_id) references species(species_id)
    -> );
Query OK, 0 rows affected (0.04 sec)


mysql> desc interests;
+------------+------+------+-----+---------+-------+
| Field      | Type | Null | Key | Default | Extra |
+------------+------+------+-----+---------+-------+
| animal_id  | int  | NO   | PRI | NULL    |       |
| species_id | int  | NO   | PRI | NULL    |       |
+------------+------+------+-----+---------+-------+
2 rows in set (0.00 sec)

------------------------------------------------------ INSERTION --------------------------------------------------------


mysql> insert into species(species,friendly_name) values('Pentalagus','Bunny');
Query OK, 1 row affected (0.01 sec)

mysql> select * from species;
+------------+------------+---------------+
| species_id | species    | friendly_name |
+------------+------------+---------------+
|          1 | Pentalagus | Bunny         |
+------------+------------+---------------+
1 row in set (0.00 sec)

mysql>  insert into animals(name,species_id,contact_email) values('Sally',1,'sally@gmail.com'), ('Franklin', 1,'franklin@gmail.com');
Query OK, 2 rows affected (0.01 sec)
Records: 2  Duplicates: 0  Warnings: 0

mysql> select * from animals;
+-----------+----------+------------+--------------------+
| animal_id | name     | species_id | contact_email      |
+-----------+----------+------------+--------------------+
|         1 | Sally    |          1 | sally@gmail.com    |
|         2 | Franklin |          1 | franklin@gmail.com |
+-----------+----------+------------+--------------------+
2 rows in set (0.00 sec)


------------------------------------------------------ SELECTION --------------------------------------------------------


mysql> select id, name, species_id, contact_email from animals;
ERROR 1054 (42S22): Unknown column 'id' in 'field list'
mysql> select animal_id, name, species_id, contact_email from animals;
+-----------+----------+------------+--------------------+
| animal_id | name     | species_id | contact_email      |
+-----------+----------+------------+--------------------+
|         1 | Sally    |          1 | sally@gmail.com    |
|         2 | Franklin |          1 | franklin@gmail.com |
+-----------+----------+------------+--------------------+
2 rows in set (0.00 sec)